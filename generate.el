;; Set the package installation directory so that packages aren't stored in the
;; ~/.emacs.d/elpa path.
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(package-install "htmlize")

;; here the config
(require 'ox-publish)
(setq org-publish-project-alist
      '(
		;; notes component
		("org-blog"
		 :base-directory "./org"
		 :base-extension "org"
		 :publishing-directory "./public_html/"
		 :recursive t
		 :headline-levels 4             ; Just the default for this project.
		 :auto-preamble t
		 :auto-sitemap t                ; Generate sitemap.org automagically...
		 :html-postamble nil
		 :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
		 :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
		 :with-autor nil
		 :with-creator nil
		 :with-toc t
		 :publishing-function org-html-publish-to-html
		 )
		("org-static-css"
		 :base-directory ".org/static/css/"
		 :base-extension "css\\"
		 :publishing-directory "./public_html/static/css/"
		 :recursive t
		 :publishing-function org-publish-attachment
		 )
		("org-static-js"
		 :base-directory "./org/static/js/"
		 :base-extension "js\\"
		 :publishing-directory "./public_html/static/js/"
		 :recursive t
		 :publishing-function org-publish-attachment
		 )
		("org-static-img"
		 :base-directory "./org/static/img/"
		 :base-extension "png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
		 :publishing-directory "./public_html/static/img/"
		 :recursive t
		 :publishing-function org-publish-attachment
		 )
		("org-post-img"
		 :base-directory "./org/img/"
		 :base-extension "png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
		 :publishing-directory "./public_html/img/"
		 :recursive t
		 :publishing-function org-publish-attachment
		 )
		("org" :components ("org-blog" "org-static-css"
							"org-static-js"  "org-static-img" "org-post-img"))
       ;; ... add all the components here (see below)...

      ))
;; customize links
(setq org-html-validation-link nil
	  org-html-head-include-scripts nil ;; usar propios scripts
	  org-html-head-include-default-style nil ;; usar propios estilos
	  org-html-head "<link rel=\"stylesheet\" 
href=\"https://cdn.simplecss.org/simple.min.css\" />"
)
;; Generate the site output
(org-publish-all t)
; HTMLIZE SOME
;; (setq org-html-htmlize-font-prefix "org-")
;; (setq org-html-htmlize-output-type 'css)
;; (setq org-src-fontify-natively t)
;; (setq org-highlight-latex-and-related '(latex script entities))

(message "loaded org-publish web")

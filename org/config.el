(require 'ox-publish)
(setq org-publish-project-alist
      '(
		;; notes component
		("org-blog"
		 :base-directory "./org/"
		 :base-extension "org"
		 :publishing-directory "./public_html/"
		 :recursive t
		 :publishing-function org-html-publish-to-html
		 :headline-levels 4             ; Just the default for this project.
		 :auto-preamble t
		 :auto-sitemap t                ; Generate sitemap.org automagically...
		 :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
		 :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
		 )
		("org-static-css"
		 :base-directory "./org/static/css/"
		 :base-extension "css\\"
		 :publishing-directory "./public_html/"
		 :recursive t
		 :publishing-function org-publish-attachment
		 )
		("org-static-js"
		 :base-directory "./org/static/js/"
		 :base-extension "js\\"
		 :publishing-directory "./public_html/"
		 :recursive t
		 :publishing-function org-publish-attachment
		 )
		("org-static-img"
		 :base-directory "./org/static/img/"
		 :base-extension "png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf|img\\"
		 :publishing-directory "./public_html/"
		 :recursive t
		 :publishing-function org-publish-attachment
		 )
		("org" :components ("org-blog" "org-static-css"
							"org-static-js"  "org-static-img"))

       ;; ... add all the components here (see below)...

      ))
